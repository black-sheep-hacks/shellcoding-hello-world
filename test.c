#include <stdio.h>
#include <string.h>

unsigned char code[] = \
"\xeb\x17\x5e\x48\x31\xc0\xb0\x01\x48\x31\xd2\xb2\x0e\x0f\x05\x48\x31\xc0\x48\x89\xc7\xb0\x3c\x0f\x05\xe8\xe4\xff\xff\xff\x48\x65\x6c\x6c\x6f\x2c\x20\x77\x6f\x72\x6c\x64\x21\x0a";

int main() {
    printf("Shellcode length: %d\n", strlen(code));
    int (*func)() = (int(*)())code;
    func();
    return 0;
}