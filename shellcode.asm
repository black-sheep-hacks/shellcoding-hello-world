global _start

section .text

_start:
    jmp call_shellcode

shellcode:
    pop rsi
    mov rax, 1
    mov rdx, 14
    syscall

    mov rax, 60
    mov rdi, 0
    syscall

call_shellcode:
    call shellcode
    hello_world: db 'Hello, world!', 0xa