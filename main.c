#include <unistd.h>
#include <stdlib.h>

int main() {
    char hello_world[] = "Hello, world!\n";
    size_t len = 14;
    write(1, hello_world, len);
    exit(0);
}